﻿using NeoAuthServer.Domain.Interfaces.Services;
using System;

namespace NeoAuthServer.Domain.Services
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        public int Add(TEntity obj)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public TEntity GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int Remove(TEntity obj)
        {
            throw new NotImplementedException();
        }

        public int Update(TEntity obj)
        {
            throw new NotImplementedException();
        }
    }
}