﻿using Microsoft.AspNetCore.Identity;

namespace NeoAuthServer.Domain.Builders
{
    public static class IdentityOptionsBuilder
    {
        public static IdentityOptions GetIdentityOptions(NivelSeguranca nivelSenha, NivelSeguranca nivelSignin)
        {
            var identityOptions = new IdentityOptions();
            identityOptions.Password = GetPasswordSettings(nivelSenha);
            identityOptions.SignIn = GetSigninSetting(nivelSignin);
            //identityOptions.
            return identityOptions;
        }

        private static SignInOptions GetSigninSetting(NivelSeguranca nivelSeguranca)
        {
            var signinSettings = new SignInOptions();
            switch (nivelSeguranca)
            {
                case NivelSeguranca.Baixo:
                    signinSettings.RequireConfirmedEmail = false;
                    signinSettings.RequireConfirmedPhoneNumber = false;
                    break;

                case NivelSeguranca.Medio:
                    signinSettings.RequireConfirmedEmail = true;
                    signinSettings.RequireConfirmedPhoneNumber = false;
                    break;

                default:
                    signinSettings.RequireConfirmedEmail = true;
                    signinSettings.RequireConfirmedPhoneNumber = true;
                    break;
            }
            return signinSettings;
        }

        private static PasswordOptions GetPasswordSettings(NivelSeguranca nivelSeguranca)
        {
            var passwordSettings = new PasswordOptions();
            switch (nivelSeguranca)
            {
                case NivelSeguranca.Baixo:
                    passwordSettings.RequireDigit = false;
                    passwordSettings.RequiredUniqueChars = 2;
                    passwordSettings.RequiredLength = 6;
                    passwordSettings.RequireLowercase = false;
                    passwordSettings.RequireNonAlphanumeric = false;
                    passwordSettings.RequireUppercase = false;
                    break;

                case NivelSeguranca.Medio:
                    passwordSettings.RequireDigit = true;
                    passwordSettings.RequiredUniqueChars = 5;
                    passwordSettings.RequiredLength = 8;
                    passwordSettings.RequireLowercase = false;
                    passwordSettings.RequireNonAlphanumeric = false;
                    passwordSettings.RequireUppercase = false;
                    break;

                default:
                    passwordSettings.RequireDigit = true;
                    passwordSettings.RequiredUniqueChars = 6;
                    passwordSettings.RequiredLength = 8;
                    passwordSettings.RequireLowercase = true;
                    passwordSettings.RequireNonAlphanumeric = true;
                    passwordSettings.RequireUppercase = true;
                    break;
            }
            return passwordSettings;
        }
    }

    public enum NivelSeguranca
    {
        Baixo = 0,
        Medio = 1,
        Alto = 2
    }
}