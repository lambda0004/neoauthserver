﻿using NeoAuthServer.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace NeoAuthServer.Domain.Builders
{
    public class ClaimBuilder
    {
        public ClaimBuilder()
        {
            Claims = new List<Claim>();
        }

        private List<Claim> Claims { get; set; }

        public List<Claim> BuildUserClaims(ApplicationUser user)
        {
            try
            {
                if (user != null)
                {
                    Claims.Add(new Claim("sub", user?.Email));
                    Claims.Add(new Claim("name", user?.UserName));
                    //Claims.Add(new Claim("idp", "local"));
                    return Claims;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Claim> BuildUserClaims(ApplicationUser user, string idp)
        {
            try
            {
                if (user != null)
                {
                    Claims.Add(new Claim("sub", user?.Email));
                    Claims.Add(new Claim("name", user?.UserName));
                    Claims.Add(new Claim("idp", idp));
                    return Claims;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}