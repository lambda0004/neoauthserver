﻿using IdentityServer4.EntityFramework.Entities;
using System.Collections.Generic;

namespace NeoAuthServer.Domain.Interfaces.Repositories
{
    public interface IClientRepository : IRepositoryBase<Client>
    {
        IEnumerable<ClientClaim> GetClientClaims(int id);

        IEnumerable<ClientClaim> GetClientClaims(string id);

        Client GetByClientId(string id);

        int RemoveAllClientClaims(Client cli);

        int AddClientClaims(IEnumerable<ClientGrantType> clientGrants, Client cli);

        int RemoveClientClaims(IEnumerable<ClientGrantType> clientGrants);
    }
}