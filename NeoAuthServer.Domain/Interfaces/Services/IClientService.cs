﻿using IdentityServer4.EntityFramework.Entities;

namespace NeoAuthServer.Domain.Interfaces.Services
{
    public interface IClientService : IServiceBase<Client>
    {
    }
}