﻿using System.Collections.Generic;

namespace NeoAuthServer.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        int Add(TEntity obj);

        TEntity GetById(int id);

        IEnumerable<TEntity> GetAll();

        int Update(TEntity obj);

        int Remove(TEntity obj);

        void Dispose();
    }
}