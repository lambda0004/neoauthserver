﻿$(document).ready(function () {
    $('.checkbox-grant').on('click', function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $(this).prop('checked', false);
        }
        else {
            $(this).prop('checked', true);
        }
        return false;
    });
    //$('.checkbox-scope').on('click', function () {
    //    fCliente.MarcarCheckbox($(this));
    //    alert('Passou por aqui!');
    //    return false;
    //});
    $('.div-grant, .label-grant').on('click', function () {
        var checkboxVal = $(this).attr('value');
        fCliente.BotaoCheckbox(checkboxVal);
        fCliente.ExibirTxtUris($(this).find('#checkbox-authorization_code'));
        return false;
    });
    $('#checkbox-authorization_code').change(function () {
        fCliente.ExibirTxtUris($(this));
        return false;
    });
    var url = window.location.pathname.split("/");
    var viewAtual = url[2];
    if (viewAtual === 'Alterar') {
        fCliente.ObterGrantTypesClient();
    }
});

var fCliente = {
    ExibirTxtUris: function (checkbox) {
        if ($(checkbox).prop('checked')) {
            $('.redirect-uri').removeClass('hidden');
            $('#RedirectUris').val('http://localhost:5002/signin-oidc');
            $('#PostLogoutRedirectUris').val('http://localhost:5002/signout-callback-oidc');
        }
        else {
            $('.redirect-uri').addClass('hidden');
            $('#RedirectUris').val('');
            $('#PostLogoutRedirectUris').val('');
        }
    },
    BotaoCheckbox: function (checkboxVal) {
        var checked = $("input", "[value='" + checkboxVal + "']").is(':checked');
        if (checked) {
            $("input", "[value='" + checkboxVal + "']").prop('checked', false);
        }
        else {
            $("input", "[value='" + checkboxVal + "']").prop('checked', true);
        }
        return false;
    },
    ObterGrantTypesClient: function () {
        var idcliente = $('#Id').attr("value");
        $.ajax({
            type: 'GET',
            url: '/Cliente/ObterGrantTypesClient/' + idcliente,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (retorno) {
                console.log(retorno);
                if (retorno.ret) {
                    $('.checkbox-grant').each(function (index, val) {
                        $(this).prop('checked', false);
                    });
                    $.each(retorno.ret, function (i, cliente) {
                        $('[value="' + cliente.grantType.toString() + '"]').prop('checked', true);
                    });
                } else
                    alert("Erro!");
            },
            error: function (request, status, error) {
                alert("erro");
            }
        });
    },
    CadastrarCliente: function () {
        var listaGrants = [];
        var grant = "";
        var listaSecrets = [];
        var listaScopes = [];
        var iteracoes = $("input:checked").length;
        $("input:checked").each(function (index, val) {
            grant = $(val).attr('value');
            listaGrants[index] = grant.toString();
        });
        $('.linha-senha').each(function (index, val) {
            var secret = {
                description: $(val).attr('data-descricao'),
                value: $(val).attr('data-senha'),
                expiration: $(val).attr('data-validade')
            };
            listaSecrets[index] = secret;
        });

        $('.linha-escopo').each(function (index, val) {
            var scope = {
                value: $(val).attr('data-escopo'),
                description: $(val).attr('data-descricao'),
                expiration: $(val).attr('data-validade')
            };
            listaScopes[index] = $(val).attr('data-escopo');
        });

        var cliente = {
            ClientName: $('#ClientName').val(),
            ClientSecrets: listaSecrets,
            RedirectUris: [$('#RedirectUris').val()],
            PostLogoutRedirectUris: [$('#PostLogoutRedirectUris').val()],
            AllowedGrantTypes: listaGrants,
            AllowedScopes: listaScopes
        };
        $.ajax({
            type: 'POST',
            url: '/Cliente/Cadastrar/',
            data: JSON.stringify(cliente),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (retorno) {
                alert("Sucesso");
                console.log(cliente.toString());
            },
            error: function (request, status, error) {
                console.log("Request:" + request + " \nStatus: " + status
                    + " \nError: " + error + "\ncliente: " + JSON.stringify(cliente));
            }
        });
    },
    AlterarCliente: function () {
        var listaGrants = [];
        var grant = "";
        var jsonGrants = "";
        var iteracoes = $("input:checked").length;
        $("input:checked").each(function (index, val) {
            grant = $(val).attr('value');

            listaGrants[index] = grant.toString();
        });
        if (listaGrants !== null) {
            //jsonGrants = '[';
            var item = '';
            $.each(listaGrants, function (index, val) {
                item = val;
                jsonGrants += item;
            });
            //jsonGrants += ']';
        }
        var cliente = {
            Id: parseInt($('#Id').attr("value")),
            ClientId: $('#ClientId').attr('value'),
            ClientName: $('#ClientName').val(),
            AllowedGrantTypes: listaGrants
        };
        $.ajax({
            type: 'POST',
            url: '/Cliente/Alterar/',
            data: JSON.stringify(cliente),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (retorno) {
                alert("Sucesso");
                console.log(cliente.toString());
            },
            error: function (request, status, error) {
                console.log("Request:" + request + " \nStatus: " + status
                    + " \nError: " + error + "\ncliente: " + JSON.stringify(cliente));
            }
        });
    },
    InserirSenha: function () {
        var senha = $('#textbox-senha').val();
        var descricao = $('#textbox-senha-descricao').val();
        var validade = $('#data-validade').val();
        if (senha === null) {
            //validação
            console.log('Senha nula');
            return false;
        }
        if (descricao === null) {
            console.log('Descrição nula');
            return false;
        }
        if (validade === null) {
            console.log('Data de valdiade nula');
            return false;
        }

        var tr = document.createElement('tr');
        tr.setAttribute('data-senha', senha);
        tr.setAttribute('data-descricao', descricao);
        tr.setAttribute('data-validade', validade);
        tr.setAttribute('class', 'linha-senha');
        var tdId = document.createElement('td');
        var tdDescricao = document.createElement('td');
        var tdValidade = document.createElement('td');
        var tdRemoveButton = document.createElement('a');

        tdId.innerHTML = '*';
        tdDescricao.innerHTML = descricao;
        tdValidade.innerHTML = validade;

        tdRemoveButton.setAttribute('class', 'btn btn-default glyphicon glyphicon-remove remove-senha');
        tdRemoveButton.setAttribute('data-senha', senha);

        tr.append(tdId);
        tr.append(tdDescricao);
        tr.append(tdValidade);
        tr.append(tdRemoveButton);
        $('#table-senha-body').append(tr);
    },
    InserirEscopo: function () {
        var escopo = $('#textbox-escopo').val();
        var descricao = $('#textbox-escopo-descricao').val();
        if (escopo === null) {
            //validação
            console.log('Escopo nulo');
            return false;
        }
        if (descricao === null) {
            console.log('Descrição nula');
            return false;
        }

        var tr = document.createElement('tr');
        tr.setAttribute('data-escopo', escopo);
        tr.setAttribute('data-descricao', descricao);
        tr.setAttribute('class', 'linha-escopo');
        var tdEscopo = document.createElement('td');
        var tdRemoveButton = document.createElement('a');

        tdEscopo.innerHTML = escopo;

        tdRemoveButton.setAttribute('class', 'btn btn-default glyphicon glyphicon-remove remove-escopo');
        tdRemoveButton.setAttribute('data-escopo', escopo);

        tr.append(tdEscopo);
        tr.append(tdRemoveButton);
        $('#table-escopo-body').append(tr);
    }

    //ObterClientes: function () {
    //    $.ajax({
    //        type: 'GET',
    //        url: '/Cliente/ObterTodos',
    //        contentType: 'application/json; charset=utf-8',
    //        dataType: 'json',
    //        success: function (retorno) {
    //            console.log(retorno);
    //            if (retorno.ret) {
    //            } else

    //        },
    //        error: function (request, status, error) {
    //        }
    //    });

    //},
};