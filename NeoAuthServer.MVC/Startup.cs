﻿using AutoMapper;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NeoAuthServer.Domain.Entities;
using NeoAuthServer.Domain.Interfaces.Repositories;
using NeoAuthServer.Infra.Data.Context;
using NeoAuthServer.Infra.Data.Repositories;
using NeoAuthServer.MVC.Services;
using System;
using System.Linq;
using System.Reflection;

//COMO CRIAR O PROJETO
//dotnet new mvc --auth Individual
namespace NeoAuthServer.MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration["ConnectionStrings:NeoAuthServerSqlServer"];
            var migrationsAssembly = typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetName().Name;
            var migrationsAssemblyPersistedGrantStore = typeof(PGrantContext).GetTypeInfo().Assembly.GetName().Name;
            var migrationsAssemblyConfigurationStore = typeof(ConfigDbContext).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseSqlServer(connectionString,
                    sqlop => sqlop.MigrationsAssembly(migrationsAssembly));
                },
                ServiceLifetime.Scoped
            );

            services.AddDbContext<ConfigDbContext>(options =>
                {
                    options.UseSqlServer(connectionString,
                    sqlop => sqlop.MigrationsAssembly(migrationsAssembly));
                },
                ServiceLifetime.Scoped
            );

            services.AddDbContext<PGrantContext>(options =>
            {
                options.UseSqlServer(connectionString,
                sqlop => sqlop.MigrationsAssembly(migrationsAssembly));
            },
                ServiceLifetime.Scoped
            );

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddIdentityServer()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays(1);
                options.SlidingExpiration = true;
            });

            services.AddTransient<IEmailSender, EmailSender>();

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddAspNetIdentity<ApplicationUser>()
                .AddConfigurationStore(options =>
                {
                    options.DefaultSchema = "dbo";
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString,
                            sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.DefaultSchema = "dbo";
                    options.ConfigureDbContext = builder =>
                       builder.UseSqlServer(connectionString,
                           sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly));
                })
                .AddAspNetIdentity<ApplicationUser>();
                //.AddInMemoryApiResources(Config.GetApiResources())
                //.AddInMemoryClients(Config.GetClients())
                //.AddInMemoryIdentityResources(Config.GetIdentityResources());

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("Cookies")
            .AddOpenIdConnect("oidc", options =>
            {
                options.SignInScheme = "Cookies";

                options.Authority = "http://localhost:5000";
                options.RequireHttpsMetadata = false;

                options.ClientId = "mvc";
                options.SaveTokens = true;
            })
            .AddGoogle(options =>
            {
                options.SignInScheme = "Identity.External";
                options.AccessType = "offline";
                options.ClientId = Configuration["Autenticacao:Google:ClientId"];
                options.ClientSecret = Configuration["Autenticacao:Google:ClientSecret"];
            })
            .AddMicrosoftAccount(options =>
            {
                options.SignInScheme = "Identity.External";
                options.ClientId = Configuration["Autenticacao:Microsoft:ClientId"];
                options.ClientSecret = Configuration["Autenticacao:Microsoft:ClientSecret"];
            });

            services.AddAutoMapper();
            services.AddSingleton<IClientRepository, ClientRepository>();
            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseIdentityServer();

            InitializeDatabase(app);//Popula database com dados de configuração em memória do Config.cs
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetServices<DbContext>();
                serviceScope.ServiceProvider.GetRequiredService<PGrantContext>().Database.Migrate();

                var context = serviceScope.ServiceProvider.GetRequiredService<ConfigDbContext>();
                context.Database.Migrate();
                if (!context.Clients.Any())
                {
                    foreach (var client in Config.GetClients())
                    {
                        context.Clients.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.IdentityResources.Any())
                {
                    foreach (var resource in Config.GetIdentityResources())
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.ApiResources.Any())
                {
                    foreach (var resource in Config.GetApiResources())
                    {
                        context.ApiResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}