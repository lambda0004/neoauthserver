﻿using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.ManageViewModels
{
    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "A senha precisa ter no mínimo {2} e no máximo {1} characteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nova senha")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmação de nova senha")]
        [Compare("NewPassword", ErrorMessage = "A nova senha e a confirmação estão diferentes")]
        public string ConfirmPassword { get; set; }

        public string StatusMessage { get; set; }
    }
}