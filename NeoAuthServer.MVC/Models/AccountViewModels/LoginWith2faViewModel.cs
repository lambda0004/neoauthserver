﻿using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.AccountViewModels
{
    public class LoginWith2faViewModel
    {
        [Required]
        [StringLength(7, ErrorMessage = "A {0} precisa ter no mínimo {2} e no máximo {1} caracteres", MinimumLength = 6)]
        [DataType(DataType.Text)]
        [Display(Name = "Código verificador")]
        public string TwoFactorCode { get; set; }

        [Display(Name = "Lembrar esta máquina")]
        public bool RememberMachine { get; set; }

        public bool RememberMe { get; set; }
    }
}