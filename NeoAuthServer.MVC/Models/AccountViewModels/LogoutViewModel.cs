﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeoAuthServer.MVC.Models.AccountViewModels
{
    public class LogoutViewModel
    {
        public string Id { get; set; }
    }
}
