using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}