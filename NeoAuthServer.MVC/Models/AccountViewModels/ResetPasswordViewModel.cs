﻿using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "A senha precisa ter no mínimo {2} e no máximo {1} characteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmação de senha")]
        [Compare("Password", ErrorMessage = "A senha e a confirmação estão diferentes")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}