﻿using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Lembrar deste dispositivo")]
        public bool RememberMe { get; set; }
    }
}