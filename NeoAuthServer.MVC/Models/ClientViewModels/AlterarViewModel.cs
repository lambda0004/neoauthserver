﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NeoAuthServer.MVC.Models.ClientViewModels
{
    public class AlterarViewModel
    {
        //public RegisterViewModel()
        //{
        //    AbsoluteRefreshTokenLifetime = TimeSpan.FromDays(30).Seconds;
        //}

        //Default 2592000 / 30 dias
        //public int AbsoluteRefreshTokenLifetime { get; set; }

        public int Id { get; set; }
        public string ClientId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string ClientName { get; set; } //dbo.client

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Tipos de Concessão")]
        public List<string> AllowedGrantTypes { get; set; } //dbo.client_grant_type

        [Display(Name = "Escopos de Acesso")]
        public List<string> AllowedScopes { get; set; }
    }
}