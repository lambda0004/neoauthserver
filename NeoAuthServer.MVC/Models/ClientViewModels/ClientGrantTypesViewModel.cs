﻿namespace NeoAuthServer.MVC.Models.ClientViewModels
{
    public class ClientGrantTypesViewModel
    {
        public int Id { get; set; }
        public string IdCliente { get; set; }
        public string GrantType { get; set; }
    }
}