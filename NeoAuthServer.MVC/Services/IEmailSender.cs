﻿using System.Threading.Tasks;

namespace NeoAuthServer.MVC.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}