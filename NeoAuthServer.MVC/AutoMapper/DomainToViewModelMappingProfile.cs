﻿using AutoMapper;
using IdentityServer4.Models;
using NeoAuthServer.MVC.Models.ClientViewModels;

namespace NeoAuthServer.MVC.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile() : this("DomainToViewModelMappingProfile")
        {
        }

        protected DomainToViewModelMappingProfile(string profileName) : base(profileName)
        {
            CreateMap<ClienteViewModel, Client>();
        }
    }
}