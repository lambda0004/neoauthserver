﻿using AutoMapper;
using IdentityServer4.Models;
using NeoAuthServer.MVC.Models.ClientViewModels;

namespace NeoAuthServer.MVC.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile() : this("ViewModelToDomainMappingProfile")
        {
        }

        public ViewModelToDomainMappingProfile(string profileName) : base(profileName)
        {
            CreateMap<ClienteViewModel, Client>();
        }
    }
}