﻿//using IdentityServer4.EntityFramework.Entities; Não use isto
//using IdentityServer4.Models;
using AutoMapper;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using NeoAuthServer.Domain.Interfaces.Repositories;
using NeoAuthServer.MVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NeoAuthServer.MVC.Controllers
{
    [Authorize]
    public class ClienteController : Controller
    {
        private readonly IClientStore _clientStore;

        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IClientRepository _clientRepository;

        public ClienteController(
            IEmailSender emailSender,
            ILogger<ClienteController> logger,
            IClientStore clientStore,
            IMapper mapper,
            IClientRepository clientRepository
            )
        {
            _emailSender = emailSender;
            _logger = logger;
            _clientStore = clientStore;
            _mapper = mapper;
            _clientRepository = clientRepository;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            ViewData["grants"] = DicionarioGrants;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var lista = _clientRepository.GetAll();

            return View(lista);
        }

        [HttpGet]
        public JsonResult ObterTodos()
        {
            var lista = _clientRepository.GetAll();
            var clientes = from c in lista
                           select new { c.Id, c.ClientName };
            return new JsonResult(new { ret = clientes });
        }

        [HttpGet]
        public IActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Cadastrar([FromBody]Client model)
        {
            if (model != null)
            {
                if (model.ClientSecrets != null && model.ClientSecrets.Count > 0)
                {
                    var senhas = new List<Secret>();
                    foreach (var secret in model.ClientSecrets)
                    {
                        secret.Value = secret.Value.Sha256();
                        senhas.Add(secret);
                    }
                    model.ClientSecrets = senhas;
                }
                var cliente = model.ToEntity();
                cliente.ClientId = Guid.NewGuid().ToString();

                var result = _clientRepository.Add(cliente);
                if (result > 0)
                {
                    return RedirectToAction("Alterar", new { id = cliente.Id });
                }
                else
                {
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError("AllowedGrantTypes", "Erro ao cadastrar a aplicação cliente. Combinação inválida de tipos de concessões, selecione no máximo 2.");
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult Alterar(int id)
        {
            var cliente = _clientRepository.GetById(id);
            //var grants = _configDbContext.ClientGrantTypes.Where(g => g.Client.Id == cliente.Id);
            //var lista = new Dictionary<string, string>();
            //if (grants != null && grants.Count() > 0) {
            //    foreach (var item in grants)
            //    {
            //        dicGrants.Add(item.GrantType, "");
            //    }
            //}
            //var scopes = _clientRepository.ClientScopes.Where(s => s.Client.Id == cliente.Id);
            //var dicScopes = new Dictionary<string, string>();
            //if (scopes != null && scopes.Count() > 0)
            //{
            //    foreach (var item in scopes)
            //    {
            //        dicScopes.Add(item.Scope, "");
            //    }
            //}
            return View(cliente.ToModel());
        }

        [HttpGet]
        public JsonResult Teste()
        {
            var cliente = _clientRepository.GetByClientId("mvc").ToModel();
            return new JsonResult(new { cliente });

            #region Client serializado, somente um exemplo

            //            {
            //                "id": 1,
            //	"enabled": true,
            //	"clientId": "client",
            //	"normalizedClientId": null,
            //	"protocolType": "oidc",
            //	"clientSecrets": null,
            //	"requireClientSecret": true,
            //	"clientName": "Client",
            //	"description": null,
            //	"clientUri": null,
            //	"logoUri": null,
            //	"requireConsent": true,
            //	"allowRememberConsent": true,
            //	"alwaysIncludeUserClaimsInIdToken": false,
            //	"allowedGrantTypes": [{
            //		"id": 1,
            //		"grantType": "client_credentials"

            //    }, {
            //		"id": 5,
            //		"grantType": "authorization_code"
            //	}, {
            //		"id": 7,
            //		"grantType": "implicit"
            //	}, {
            //		"id": 8,
            //		"grantType": "urn:ietf:params:oauth:grant-type:jwt-bearer"
            //	}, {
            //		"id": 9,
            //		"grantType": "password"
            //	}],
            //	"requirePkce": false,
            //	"allowPlainTextPkce": false,
            //	"allowAccessTokensViaBrowser": false,
            //	"redirectUris": null,
            //	"postLogoutRedirectUris": null,
            //	"frontChannelLogoutUri": null,
            //	"frontChannelLogoutSessionRequired": true,
            //	"backChannelLogoutUri": null,
            //	"backChannelLogoutSessionRequired": true,
            //	"allowOfflineAccess": false,
            //	"allowedScopes": null,
            //	"identityTokenLifetime": 300,
            //	"accessTokenLifetime": 3600,
            //	"authorizationCodeLifetime": 300,
            //	"consentLifetime": null,
            //	"absoluteRefreshTokenLifetime": 2592000,
            //	"slidingRefreshTokenLifetime": 1296000,
            //	"refreshTokenUsage": 1,
            //	"updateAccessTokenClaimsOnRefresh": false,
            //	"refreshTokenExpiration": 1,
            //	"accessTokenType": 0,
            //	"enableLocalLogin": true,
            //	"identityProviderRestrictions": null,
            //	"includeJwtId": false,
            //	"claims": null,
            //	"alwaysSendClientClaims": false,
            //	"prefixClientClaims": true,
            //	"allowedCorsOrigins": null,
            //	"properties": null
            //}

            #endregion Client serializado, somente um exemplo
        }

        [HttpGet]
        public JsonResult Teste2()
        {
            return new JsonResult(new
            {
                GrantTypes.ClientCredentials,
                GrantTypes.Code,
                GrantTypes.CodeAndClientCredentials,
                GrantTypes.Hybrid,
                GrantTypes.HybridAndClientCredentials,
                GrantTypes.Implicit,
                GrantTypes.ImplicitAndClientCredentials,
                GrantTypes.ResourceOwnerPassword,
                GrantTypes.ResourceOwnerPasswordAndClientCredentials
            });
        }

        [HttpPost]
        public IActionResult Alterar([FromBody]Client model)//[FromBody]RegisterViewModel cliente
        {
            var success = _clientRepository.Update(model.ToEntity());
            if (success > 0)
            {
                return View(model);
            }
            return View();
        }

        public Dictionary<string, string> DicionarioGrants
        {
            get
            {
                var grants = new Dictionary<string, string>()
                {
                    { GrantType.AuthorizationCode, "Authorization Code" },
                    { GrantType.ClientCredentials, "Client Credentials"},
                    { GrantType.Hybrid, "Hybrid"},
                    { GrantType.Implicit, "Implicit" },
                    { GrantType.ResourceOwnerPassword, "Password"}
                };
                return grants;
            }
            //Veja abaixo as diferenças entre GrantType e GrantTypes. tl;dr: GrantTypes é uma lista (muitas vezes de um único item) de GrantType.

            #region GrantType

            //{ "clientCredentials":"client_credentials","authorizationCode":"authorization_code","hybrid":"hybrid","implicit":"implicit","resourceOwnerPassword":"password"}

            #endregion GrantType

            #region GrantTypes

            //{ "gts":["client_credentials"],"code":["authorization_code"],"codeAndClientCredentials":["authorization_code","client_credentials"],"hybrid":["hybrid"],"hybridAndClientCredentials":["hybrid","client_credentials"],"implicit":["implicit"],"implicitAndClientCredentials":["implicit","client_credentials"],"resourceOwnerPassword":["password"],"resourceOwnerPasswordAndClientCredentials":["password","client_credentials"]}

            #endregion GrantTypes
        }
    }
}