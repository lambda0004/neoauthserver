﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace NeoAuthServer.Infra.Data.Context
{
    public class PGrantContextFactory : IDesignTimeDbContextFactory<PGrantContext>
    {
        public PGrantContext CreateDbContext(string[] args)
        {
            var relativePath = @"../NeoAuthServer.MVC/";
            var absolutePath = System.IO.Path.GetFullPath(relativePath);

            var builder = new DbContextOptionsBuilder<PersistedGrantDbContext>();
            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(absolutePath)
              .AddJsonFile("appsettings.json")
              .Build();
            builder.UseSqlServer(configuration.GetConnectionString("NeoAuthServerSqlServer"));

            OperationalStoreOptions storeOptions = new OperationalStoreOptions();
            //storeOptions.ApiClaim
            return new PGrantContext(builder.Options, storeOptions);
        }
    }
}