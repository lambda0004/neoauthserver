﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NeoAuthServer.Domain.Entities;

namespace NeoAuthServer.Infra.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>(b =>
            {
                b.ToTable("asp_users");
                //b.HasKey(e => e.Id);
                b.HasMany(e => e.Claims)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

                b.HasMany(e => e.Logins)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

                b.HasMany(e => e.Roles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            })
            .Entity<IdentityRole>(b =>
            {
                b.ToTable("asp_roles");
            })
            .Entity<IdentityRoleClaim<string>>(b =>
            {
                b.ToTable("asp_role_claims");
            })
            .Entity<IdentityUserClaim<string>>(b =>
            {
                b.ToTable("asp_user_claims");
            })
            .Entity<IdentityUserLogin<string>>(b =>
            {
                b.ToTable("asp_user_logins");
                //b.HasKey(e => new { e.LoginProvider, e.ProviderKey});
            })
            .Entity<IdentityUserRole<string>>(b =>
            {
                b.ToTable("asp_user_roles");
                //b.HasKey(e => new { e.RoleId, e.UserId});
            })
            .Entity<IdentityUserToken<string>>(b =>
            {
                b.ToTable("asp_user_tokens");
            });
        }
    }
}