﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;

namespace NeoAuthServer.Infra.Data.Context
{
    public class PGrantContext : PersistedGrantDbContext
    {
        public PGrantContext(DbContextOptions<PersistedGrantDbContext> options, OperationalStoreOptions storeOptions)
            : base(options, storeOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //builder.Entity<PersistedGrant>(b =>
            //{
            //    b.ToTable("persisted_grants");
            //});
        }
    }
}