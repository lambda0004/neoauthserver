﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace NeoAuthServer.Infra.Data.Context
{
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        //http://benjii.me/2017/05/enable-entity-framework-core-migrations-visual-studio-2017/

        public ApplicationDbContext CreateDbContext(string[] args)
        {
            #region ConnectionString chumbada

            //var builder = new DbContextOptionsBuilder<NeoAuthServerSqlServerContext>();
            //builder.UseSqlServer("Server=LAMBDA4;Database=NeoAuthServer;Trusted_Connection=True;MultipleActiveResultSets=true",
            //    optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(NeoAuthServerSqlServerContext).GetTypeInfo().Assembly.GetName().Name));
            //return new NeoAuthServerSqlServerContext(builder.Options);

            #endregion ConnectionString chumbada

            //http://benjii.me/2017/05/enable-entity-framework-core-migrations-visual-studio-2017/
            //https://stackoverflow.com/questions/45892312/how-to-add-an-implementation-of-idesigntimedbcontextfactorydatacontext-to-th

            var relativePath = @"../NeoAuthServer.MVC/";
            var absolutePath = System.IO.Path.GetFullPath(relativePath);

            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(absolutePath)
              .AddJsonFile("appsettings.json")
              .Build();

            builder.UseSqlServer(configuration.GetConnectionString("NeoAuthServerSqlServer"));
            return new ApplicationDbContext(builder.Options);
        }
    }
}