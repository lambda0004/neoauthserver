﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace NeoAuthServer.Infra.Data.Context
{
    public class ConfigDbContextFactory : IDesignTimeDbContextFactory<ConfigDbContext>
    {
        public ConfigDbContext CreateDbContext(string[] args)
        {
            var relativePath = @"../NeoAuthServer.MVC/";
            var absolutePath = System.IO.Path.GetFullPath(relativePath);

            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            IConfigurationRoot configuration = new ConfigurationBuilder()
              .SetBasePath(absolutePath)
              .AddJsonFile("appsettings.json")
              .Build();
            builder.UseSqlServer(configuration.GetConnectionString("NeoAuthServerSqlServer"));

            ConfigurationStoreOptions storeOptions = new ConfigurationStoreOptions();
            //storeOptions.ApiClaim
            return new ConfigDbContext(builder.Options, storeOptions);
        }
    }
}