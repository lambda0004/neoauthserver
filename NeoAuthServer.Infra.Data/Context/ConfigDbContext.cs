﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;

namespace NeoAuthServer.Infra.Data.Context
{
    public class ConfigDbContext : ConfigurationDbContext
    {
        public ConfigDbContext(DbContextOptions<ConfigurationDbContext> options, ConfigurationStoreOptions storeOptions)
            : base(options, storeOptions)
        {
        }

        //public DbSet<IdentityResource> IdentityResources { get; set; }
        public DbSet<ClientGrantType> ClientGrantTypes { get; set; }

        public DbSet<ClientScope> ClientScopes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //builder.Entity<ApiResource>(b =>
            //{
            //    b.ToTable("api_resource");
            //})
            //.Entity<ApiResourceClaim>(b =>
            //{
            //    b.ToTable("api_resource_claim");
            //})
            //.Entity<ApiScope>(b =>
            //{
            //    b.ToTable("api_scope");
            //})
            //.Entity<ApiScopeClaim>(b =>
            //{
            //    b.ToTable("api_scope_claim");
            //})
            //.Entity<ApiSecret>(b =>
            //{
            //    b.ToTable("api_secret");
            //})
            //.Entity<Client>(b =>
            //{
            //    b.ToTable("client");
            //})
            //.Entity<ClientClaim>(b =>
            //{
            //    b.ToTable("client_claim");
            //})
            //.Entity<ClientCorsOrigin>(b =>
            //{
            //    b.ToTable("client_cors_origin");
            //})
            //.Entity<ClientGrantType>(b =>
            //{
            //    b.ToTable("client_grant_type");
            //})
            //.Entity<ClientIdPRestriction>(b =>
            //{
            //    b.ToTable("clientid_grant_type");
            //})
            //.Entity<ClientPostLogoutRedirectUri>(b =>
            //{
            //    b.ToTable("client_post_logout_redirect_uri");
            //})
            //.Entity<ClientProperty>(b =>
            //{
            //    b.ToTable("client_property");
            //})
            //.Entity<ClientRedirectUri>(b =>
            //{
            //    b.ToTable("client_redirect_uri");
            //})
            //.Entity<ClientScope>(b =>
            //{
            //    b.ToTable("client_scope");
            //})
            //.Entity<ClientSecret>(b =>
            //{
            //    b.ToTable("client_secret");
            //})
            //.Entity<IdentityClaim>(b =>
            //{
            //    b.ToTable("identity_claim");
            //})
            //.Entity<IdentityResource>(b =>
            //{
            //    b.ToTable("identity_resource");
            //});
        }
    }
}