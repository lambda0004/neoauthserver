﻿using IdentityServer4.EntityFramework.Entities;
using Microsoft.EntityFrameworkCore;
using NeoAuthServer.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NeoAuthServer.Infra.Data.Repositories
{
    public class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
        public int AddClientClaims(IEnumerable<ClientGrantType> clientGrants, Client cli)
        {
            try
            {
                var removerGrants = _configDbContext.ClientGrantTypes.Where(c => c.Client.Id == cli.Id).ToList();
                _configDbContext.ClientGrantTypes.RemoveRange(removerGrants);

                _configDbContext.ClientGrantTypes.AddRange(clientGrants);
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Client GetByClientId(string id)
        {
            try
            {
                var client = _configDbContext.Clients.
                    Include(c => c.AllowedGrantTypes).
                    Include(c => c.AllowedScopes).
                    Include(c => c.ClientSecrets).
                    Include(c => c.Claims).
                    FirstOrDefault(c => c.ClientId == id);
                return client != null ? client : new Client() { ClientName = "Cliente não encontrado." };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ClientClaim> GetClientClaims(int id)
        {
            try
            {
                var client = _configDbContext.Clients.Include(c => c.Claims).FirstOrDefault(c => c.Id == id);
                if (client != null && client.Claims != null)
                {
                    return client.Claims;
                }
                return new List<ClientClaim>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ClientClaim> GetClientClaims(string id)
        {
            try
            {
                var client = _configDbContext.Clients.Include(c => c.Claims).FirstOrDefault(c => c.ClientId == id);
                if (client != null && client.Claims != null)
                {
                    return client.Claims;
                }
                return new List<ClientClaim>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RemoveAllClientClaims(Client cli)
        {
            try
            {
                var clientGrants = _configDbContext.ClientGrantTypes.Where(c => c.Client.Id == cli.Id).ToList();
                _configDbContext.ClientGrantTypes.RemoveRange(clientGrants);
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RemoveClientClaims(IEnumerable<ClientGrantType> clientGrants)
        {
            try
            {
                _configDbContext.ClientGrantTypes.RemoveRange(clientGrants);
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}