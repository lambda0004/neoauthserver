﻿using Microsoft.EntityFrameworkCore;
using NeoAuthServer.Domain.Interfaces.Repositories;
using NeoAuthServer.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NeoAuthServer.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected ConfigDbContext _configDbContext = (new ConfigDbContextFactory()).CreateDbContext(null);

        public int Add(TEntity obj)
        {
            try
            {
                _configDbContext.Set<TEntity>().Add(obj);
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            //TODO: Implementar dispose
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return _configDbContext.Set<TEntity>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TEntity GetById(int id)
        {
            try
            {
                return _configDbContext.Set<TEntity>().Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Remove(TEntity obj)
        {
            try
            {
                _configDbContext.Set<TEntity>().Remove(obj);
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(TEntity obj)
        {
            try
            {
                _configDbContext.Set<TEntity>().Attach(obj);
                _configDbContext.Entry(obj).State = EntityState.Modified;
                return _configDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}