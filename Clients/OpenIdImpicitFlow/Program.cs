﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;

namespace OpenIdImpicitFlow
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "OpenIdConnect Implicit Flow";
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}