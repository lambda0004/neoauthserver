﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ConsoleClient
{
    public class Program
    {
        private const string autoridade = "http://localhost:5000";
        private const string api = "http://localhost:5001";

        private static async Task Main()
        {
            Console.Title = "Console Client Credentials Flow";
            Console.ReadKey();
            while (true)
            {
                var response = await RequestTokenAsync();
                Console.WriteLine("Requisição do Token.....");
                Console.WriteLine($"Request Servidor:{response.AccessToken.ToString()}\n");
                Console.WriteLine("GET na API...............");
                await CallServiceAsync(response.AccessToken);
                Console.ReadLine();
            }
        }

        private static async Task<TokenResponse> RequestTokenAsync()
        {
            var disco = await DiscoveryClient.GetAsync(autoridade);
            if (disco.IsError) throw new Exception(disco.Error);

            var client = new TokenClient(disco.TokenEndpoint, "client", "secret");

            var optional = new
            {
                acr_values = "tenant:custom_account_store1 foo bar quux"
            };

            var token = await client.RequestClientCredentialsAsync("api1");
            return token;
        }

        private static async Task CallServiceAsync(string token)
        {
            var baseAddress = api;

            var client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };

            client.SetBearerToken(token);
            var response = await client.GetAsync("identity");
            string retorno = response.ToString();
            //"\n\nService claims:".ConsoleGreen();
            try
            {
                Console.WriteLine("\n\nSTATUS CODE:");
                Console.WriteLine(response.StatusCode.ToString());
                Console.WriteLine("\n\nHEADER:");
                Console.WriteLine(response.Headers.ToString());
                Console.WriteLine("\n\nCONTEUDO DO REQUEST");
                Console.WriteLine(response.RequestMessage.ToString());
                Console.WriteLine("\n\nCONTEUDO DA RESPOSTA!?");
                string teste = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(teste));
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("EXCEÇÃO!");
                Console.ReadKey();
            }
        }
    }
}